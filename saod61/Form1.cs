﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace saod61
{
    public partial class Form1 : Form
    {
        private List<List<KeyValuePair<string, string>>> table;
        private string fileName;
        private List<KeyValuePair<string, string>> lst;
        

        public Form1()
        {
            InitializeComponent();
            table = new List<List<KeyValuePair<string, string>>>();
            lst = new List<KeyValuePair<string, string>>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)

            {
                fileName = openFileDialog1.FileName;
            }
            else return;

            string[] Lines = System.IO.File.ReadAllLines(@fileName);
            textBox1.Text = Lines.Length.ToString();

            foreach(var i in Lines)
            {
                string Key = i.Substring(0, i.IndexOf(" "));
                string val = i.Substring(i.IndexOf(" ") + 1);
                lst.Add(new KeyValuePair<string,string>( Key, val));

            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        public class MyComp : IComparer<KeyValuePair<string, string>>
        {
            public int Compare(KeyValuePair<string, string> x,
                                KeyValuePair<string, string> y)
            {
                return x.Key.CompareTo(y.Key);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            int n = Convert.ToInt32(textBox2.Text);
            int max = 0, min = 9999;
            double std=0, xs=0; 

         



            table.Clear();

            if (lst.Count == 0)
                return;
            for (int i = 0; i != n; i++)
            {
                table.Add(new List<KeyValuePair<string, string>>());
            }
            int[] toChart = new int[n];
            for (int i = 0; i != lst.Count; i++)
            {
                int index = 0;
                if (radioButton1.Enabled)
                    index = Convert.ToInt32(lst[i].Key) % n;

                var ind = table[index].BinarySearch(lst[i], new MyComp());
                if (ind < 0) ind = ~ind;
                table[index].Insert(ind, lst[i]);
                toChart[index] = table[index].Count;
            }

            chart1.Series[0].Points.Clear();

            xs += toChart[0] ;
            for (int i = 0; i != n; i++)
                    {
                xs += Math.Pow(xs - toChart[i],2);
                
                    }
            xs = xs / n;

            for (int i = 0; i != n; i++)
            {
                xs += (Math.Pow(xs - toChart[i], 2)/n);

            }


            for (int i = 0; i != n; i++)
            {
                chart1.Series[0].Points.AddXY(i, toChart[i]);
  

                if (max < toChart[i]) { max = toChart[i];  }
                if (min > toChart[i]) { min = toChart[i]; }

            }

            textBox3.Text = "max= "+max+" \n min = "+min+"\n std = "+ Math.Sqrt(xs);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
